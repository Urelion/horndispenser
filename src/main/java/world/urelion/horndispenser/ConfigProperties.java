package world.urelion.horndispenser;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * contains fix names or path of properties in the {@link FileConfiguration}
 *
 * @since 2.0.0
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigProperties {
	/**
	 * property name to define the range,
	 * in which {@link Player} should hear the {@link Sound}s
	 *
	 * @since 2.0.0
	 */
	public static final @NotNull String HORN_RANGE = "hornRange";
	/**
	 * property name to define the cool down (in milliseconds),
	 * to avoid horn spamming
	 *
	 * @since 2.0.0
	 */
	public static final @NotNull String COOL_DOWN  = "coolDown";
}
