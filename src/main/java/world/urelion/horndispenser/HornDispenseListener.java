package world.urelion.horndispenser;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.joda.time.DateTime;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * play {@link Sound}s on all players in range,
 * if a {@link Material#GOAT_HORN} was dispensed
 *
 * @since 1.0.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Singleton(style = Style.HOLDER)
public class HornDispenseListener
implements Listener {
	/**
	 * {@link Singleton} instance of the {@link HornDispenseListener}
	 *
	 * @since 1.0.0
	 */
	@Getter
	private static final @NotNull HornDispenseListener INSTANCE =
		new HornDispenseListener();

	/**
	 * the volume limit, volume higher than this will be expand the range
	 *
	 * @since 2.0.0
	 */
	@Getter
	private static final          double VOLUME_LIMIT = 15;
	/**
	 * {@link Map} of all current cool downs
	 *
	 * @since 1.0.0
	 */
	private static final @NotNull Map<Location, DateTime> COOL_DOWNS =
		new ConcurrentHashMap<>();

	/**
	 * play sounds to players on dispense a {@link Material#GOAT_HORN}
	 *
	 * @param event the {@link BlockDispenseEvent},
	 *              which is fired on dispensing a {@link Material#GOAT_HORN}
	 *
	 * @since 1.0.0
	 */
	@EventHandler
	public void onDispense(
		final BlockDispenseEvent event
	) {
		HornDispenseListener.log.debug("Check is event is already canceled.");
		if (event.isCancelled()) {
			HornDispenseListener.log.debug(
				"Event is canceled! Cancel event handling."
			);
			return;
		}

		HornDispenseListener.log.trace("Get lookup table.");
		final @NotNull HornSoundTable hornSoundTable =
			HornSoundTable.getINSTANCE();

		HornDispenseListener.log.trace("Define goat horn sound.");
		final @Nullable Sound sound;
		try {
			HornDispenseListener.log.trace("Get sound from lookup table.");
			sound = hornSoundTable.getSound(event.getItem());
		} catch (IllegalArgumentException e) {
			HornDispenseListener.log.debug(
				"Dispensed item is not a valid goat horn! " +
				"Cancel event handling."
			);
			return;
		}

		HornDispenseListener.log.trace("Check if the sound was found.");
		if (sound == null) {
			HornDispenseListener.log.warn(
				"Goat horn has an unknown sound! Cancel event handling."
			);
			return;
		}

		HornDispenseListener.log.debug("Cancel event to avoid item drop.");
		event.setCancelled(true);

		HornDispenseListener.log.trace("Get the dispensing block.");
		final @NotNull Block block = event.getBlock();
		HornDispenseListener.log.trace(
			"Get the location of the dispensing block."
		);
		final @NotNull Location location = block.getLocation();

		HornDispenseListener.log.trace("Get plugin instance.");
		final @Nullable HornDispenserPlugin hornDispenserPlugin =
			HornDispenserPlugin.getINSTANCE();
		HornDispenseListener.log.trace("Check if plugin is initialized.");
		if (hornDispenserPlugin == null) {
			HornDispenseListener.log.error(
				"Plugin not initialized! Cancel event handling."
			);
			return;
		}

		HornDispenseListener.log.trace("Get plugin configuration from file.");
		final @NotNull FileConfiguration fileConfig =
			hornDispenserPlugin.getConfig();

		HornDispenseListener.log.trace(
			"Define boolean to track if cool down is active"
		);
		boolean isCoolingDown = false;
		HornDispenseListener.log.trace("Get cool down from configuration.");
		final int coolDown = fileConfig.getInt(ConfigProperties.COOL_DOWN);
		HornDispenseListener.log.trace("Get current time for cool down checks.");
		final @NotNull DateTime currentTime = new DateTime();
		HornDispenseListener.log.debug("Iterate over all current cool downs.");
		for (
			final Entry<Location, DateTime> coolDownEntry :
			HornDispenseListener.COOL_DOWNS.entrySet()
		) {
			HornDispenseListener.log.trace(
				"Get location of cool down in list."
			);
			final Location coolDownLocation = coolDownEntry.getKey();

			HornDispenseListener.log.trace(
				"Get cool down time of " + location + "."
			);
			final DateTime coolDownTime = coolDownEntry.getValue();
			HornDispenseListener.log.debug(
				"Check if cool down in list is already expired."
			);
			if (coolDownTime.plusMillis(coolDown).isBefore(currentTime)) {
				HornDispenseListener.log.debug(
					"Remove expired cool down from list."
				);
				HornDispenseListener.COOL_DOWNS.remove(coolDownLocation);
			} else {
				HornDispenseListener.log.trace(
					"Check if list location is same as dispense location."
				);
				if (location.distance(coolDownLocation) < 1) {
					HornDispenseListener.log.debug(
						"Cool down is active! Set tracker."
					);
					isCoolingDown = true;
				}
			}
		}

		HornDispenseListener.log.debug("Check if cool down tracker is set.");
		if (isCoolingDown) {
			HornDispenseListener.log.debug(
				"Cool down is not expired! Cancel event handling."
			);
			return;
		}

		HornDispenseListener.log.trace("Get horn range from configuration.");
		final double hornRange = fileConfig.getDouble(
			ConfigProperties.HORN_RANGE
		);
		HornDispenseListener.log.trace("Get volume limit.");
		final double volumeLimit = HornDispenseListener.getVOLUME_LIMIT();

		HornDispenseListener.log.debug("Get all nearby entities.");
		for (final Entity entity : block.getWorld().getNearbyEntities(
			location,
			hornRange,
			hornRange,
			hornRange
		)) {
			HornDispenseListener.log.debug(
				"Check if nearby entity is a player."
			);
			if (entity instanceof Player) {
				HornDispenseListener.log.trace("Get entity as player.");
				final Player player = (Player)entity;

				HornDispenseListener.log.debug(
					"Play sound to player " + player.getName() + "."
				);
				player.playSound(
					location,
					sound,
					(float)(hornRange / volumeLimit),
					1
				);
			}
		}

		HornDispenseListener.log.debug("Start cool down at " + location + ".");
		HornDispenseListener.COOL_DOWNS.put(location, currentTime);
	}
}
