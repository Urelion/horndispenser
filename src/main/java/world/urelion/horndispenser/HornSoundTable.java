package world.urelion.horndispenser;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.Getter;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * lookup table, which gives the {@link Sound} of a {@link Material#GOAT_HORN}.
 *
 * @since 2.0.0
 */
@Slf4j
@Singleton(style = Style.HOLDER)
public class HornSoundTable {
	/**
	 * {@link Singleton} instance of the {@link HornSoundTable}
	 *
	 * @since 2.0.0
	 */
	@Getter
	private static final @NotNull HornSoundTable INSTANCE =
		new HornSoundTable();

	/**
	 * the lookup table to store the sounds
	 * by {@link Material#GOAT_HORN} instrument names
	 *
	 * @since 2.0.0
	 */
	private final @NotNull Map<String, Sound> LOOKUP_TABLE = new HashMap<>();

	/**
	 * default constructor, which initialize the lookup table
	 *
	 * @since 2.0.0
	 */
	private HornSoundTable() {
		final @NotNull String namespace = "minecraft:";
		final @NotNull String suffix    = "_goat_horn";

		HornSoundTable.log.debug("Fill lookup table of sounds.");
		this.LOOKUP_TABLE.put(
			namespace + "ponder" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_0
		);
		this.LOOKUP_TABLE.put(
			namespace + "sing" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_1
		);
		this.LOOKUP_TABLE.put(
			namespace + "seek" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_2
		);
		this.LOOKUP_TABLE.put(
			namespace + "feel" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_3
		);
		this.LOOKUP_TABLE.put(
			namespace + "admire" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_4
		);
		this.LOOKUP_TABLE.put(
			namespace + "call" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_5
		);
		this.LOOKUP_TABLE.put(
			namespace + "yearn" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_6
		);
		this.LOOKUP_TABLE.put(
			namespace + "dream" + suffix,
			Sound.ITEM_GOAT_HORN_SOUND_7
		);
	}

	/**
	 * give the {@link Sound} of a {@link Material#GOAT_HORN}
	 *
	 * @param stack the {@link ItemStack} of {@link Material#GOAT_HORN}(s)
	 * @return the sound of the given {@link Material#GOAT_HORN}
	 *         {@link ItemStack} or {@code null}
	 *         if the {@link Material#GOAT_HORN} has an unknown instrument
	 * @throws IllegalArgumentException if the given {@link ItemStack} is not
	 *                                  a valid {@link Material#GOAT_HORN}
	 *
	 * @since 2.0.0
	 */
	public @Nullable Sound getSound(
		final @NotNull ItemStack stack
	) throws IllegalArgumentException {
		HornSoundTable.log.debug("Check if the given item is a goat horn.");
		if (!(Material.GOAT_HORN.equals(stack.getType()))) {
			final @NotNull String errorMessage =
				"Given item is not a goat horn!";
			HornSoundTable.log.debug(errorMessage);
			throw new IllegalArgumentException(errorMessage);
		}

		HornSoundTable.log.debug("Check if item has meta data.");
		if (!(stack.hasItemMeta())) {
			final @NotNull String errorMessage = "Item has no meta data!";
			HornSoundTable.log.debug(errorMessage);
			throw new IllegalArgumentException(errorMessage);
		}

		HornSoundTable.log.trace("Get item meta data.");
		final @Nullable ItemMeta meta = stack.getItemMeta();
		HornSoundTable.log.debug("Check if meta data is empty.");
		if (meta == null) {
			final @NotNull String errorMessage = "Meta data doesn't exist!";
			HornSoundTable.log.debug(errorMessage);
			throw new IllegalArgumentException(errorMessage);
		}

		HornSoundTable.log.trace("Get meta data as JSON object.");
		final JsonObject json = JsonParser.parseString(
			meta.getAsString()
		).getAsJsonObject();
		HornSoundTable.log.trace("Get instrument from meta data.");
		final String instrument = json.get("instrument").getAsString();
		HornSoundTable.log.trace("Get sound by instrument from lookup table.");
		return this.LOOKUP_TABLE.get(instrument);
	}
}
