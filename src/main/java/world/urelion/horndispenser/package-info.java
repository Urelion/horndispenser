/**
 * root package of the HornDispenser plugin
 *
 * @version 1.0.0
 * @since 1.0.0
 */
package world.urelion.horndispenser;
